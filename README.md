==============================================================
RUN ON DOCKER CONTAINER
  build with docker command and run new container 
or
  build with docker-compose 
exposed port is 4001

add volume if you want to read dir from host system

==============================================================
RUN ON LOCAL SYSTEM
npm install
node index.js


use api 
localhost:4001/getdir
with payload json
{
    "dirpath": "/path/to/dir"
}
