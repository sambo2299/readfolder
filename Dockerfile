FROM node:latest

RUN mkdir -p  /home/proj/dirInfo

COPY . /home/proj/dirInfo

WORKDIR /home/proj/dirInfo

RUN npm i 

EXPOSE 4001

CMD [ "node", "index.js" ]

