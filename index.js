
const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const path = require('path');

const app = express();

app.use(bodyParser.json());

const getDirInfo = (req, res) =>  {
  const dirpath = req.body.dirpath
  fs.exists(dirpath, (exists) => {
    if(!exists) res.status(500).send('dir doesnot exists!!!');
    else {
      fs.readdir(dirpath, (err, dirContent) => {
        const dts = [];
        dirContent.forEach(itm => {
          if(itm) {
            const stats = fs.statSync(path.join(dirpath, itm));
            const obj = {
              name: itm,
              size: `${(stats.size /  (1024*1024)).toFixed(2)} mb`,

            }
            obj.type = stats.isDirectory() ? 'dir' : 'file';
            dts.push(obj);
          } 
        })
        res.json(dts);
      });
    }
  })
};

app.post('/getDir', (req, res) => {
  // console.log(req.body)
  try {
    return getDirInfo(req, res);

  } catch(ex) {
    console.log(ex);
    res.status(500).send('internal server error');  
  }
  // res.send(getDirInfo(req.body.dirpath))
  // res.send();
});
app.use((req, res) => {
  console.log('here')
  return res.status(500).send('no route')
})
const server = http.createServer(app);
server.listen(4001);  
server.on('error', (err) => {
  console.log(err);
  // throw err;
});

server.on('listening', (resp) => {
  console.log('server listening in port', server.address().port);
})
